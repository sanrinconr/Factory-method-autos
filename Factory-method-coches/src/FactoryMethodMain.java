import Marca.Concesionario;
import Pintura.*;

public class FactoryMethodMain {

	public static void main(String[] args) {
		final int  SEDAN=1;
		final int  COUPE=2;
		final int CAMIONETA=3;
		
		FabricaCoches ensambladora = new FabricaCoches();
		Concesionario auto= ensambladora.crearCoche("Porsche",2);
		
		System.out.println(auto.getDatosGenerales());
		System.out.println();

		
		
		FabricaRepintado fabricaPinturas = new FabricaRepintado();
		Color pintura= fabricaPinturas.cargarPintura("Dorado"); 
		pintura.pintar(auto, pintura);
		System.out.println("Pintando.... ");
		System.out.println();
		
		
		System.out.println(auto.getDatosGenerales());
	}
}
