package Marca;

public class Porsche extends Concesionario{


	final int  SEDAN=1;
	final int  COUPE=2;
	final int CAMIONETA=3;
	
	private int tipo;
	private String color=null;
	private String modelo;
	private String tipoS;
	
	public Porsche(int tipo) {
		switch(tipo){
		case SEDAN:
			this.tipo=tipo;
			modelo="Panamera";
			color="negro";
			tipoS="Sedan";

		case COUPE:
			this.tipo=tipo;
			modelo="911";
			color="negro";
			tipoS="Coupe";

		case CAMIONETA:
			this.tipo=tipo;
			modelo="Cayenne";
			color="negro";
			tipoS="Camioneta ";

		}
	}
	
	@Override
	public String getDatosGenerales() {
		// TODO Auto-generated method stub
		if(this.color==null) {
			return "No contamos con este tipo de auto, lo sentimos";
		}
		else 
			return "Marca: Porsche\n"
					+ "Tipo: "+tipoS+"\n"
							+ "Color: "+color+"\n"
							+"Modelo: "+modelo ;
	}
	
	public void setColor(String color) {
		// TODO Auto-generated method stub
		this.color=color;
	}
	
}
