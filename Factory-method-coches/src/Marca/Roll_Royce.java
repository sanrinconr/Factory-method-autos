package Marca;

public class Roll_Royce extends Concesionario{

	final int  SEDAN=1;
	final int  COUPE=2;
	final int CAMIONETA=3;
	
	private int tipo;
	private String color=null;
	private String modelo;
	private String tipoS;

	
	
	public Roll_Royce(int tipo) {
		switch(tipo){
		case SEDAN:
			this.tipo=tipo;
			modelo="Phantom";
			color="negro";
			tipoS="Sedan";
		case COUPE:
			this.tipo=tipo;
			modelo="Wraith";
			color="negro";
			tipoS="Coupe";

		case CAMIONETA:
			this.tipo=tipo;
			modelo="cullinan";
			color="negro";
			tipoS="Camioneta";

		}
	}
	
	@Override
	public String getDatosGenerales() {
		// TODO Auto-generated method stub
		if(this.color==null) {
			return "No contamos con este tipo de auto, lo sentimos";
		}
		else 
	
			return "Marca: Roll_Royce\n"
					+ "Tipo: "+tipoS+"\n"
							+ "Color: "+color+"\n"
							+"Modelo: "+modelo ;
	}
	
	public void setColor(String color) {
		// TODO Auto-generated method stub
		this.color=color;
	}
	
}
