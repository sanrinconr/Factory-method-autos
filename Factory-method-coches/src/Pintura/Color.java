package Pintura;

import Marca.Concesionario;

public abstract class Color {
	public abstract String getColor();

	public abstract void pintar(Concesionario auto, Color pintura);
}
