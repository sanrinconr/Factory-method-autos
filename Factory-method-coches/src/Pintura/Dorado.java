package Pintura;

import Marca.Concesionario;

public class Dorado extends Color{

	private String colorPintura= "Dorado";
	
	public void pintar(Concesionario coche, Color pintura) {

		coche.setColor(pintura.getColor());

	}
	
	public String getColor() {
		return colorPintura;
	}
}
