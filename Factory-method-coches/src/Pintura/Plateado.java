package Pintura;

import Marca.Concesionario;

public class Plateado extends Color {

	private String colorPintura= "Plateado";
	
	public void pintar(Concesionario coche, Color pintura) {

		coche.setColor(pintura.getColor());

	}
	
	public String getColor() {
		return colorPintura;
	}
}
