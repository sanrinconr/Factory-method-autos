package Pintura;

import Marca.Concesionario;

public class Rojo extends Color {

	private String colorPintura= "Rojo";
	
	public void pintar(Concesionario coche, Color pintura) {

		coche.setColor(pintura.getColor());

	}
	
	public String getColor() {
		return colorPintura;
	}
}
