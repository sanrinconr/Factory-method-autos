package Pintura;

import Marca.Concesionario;

public class Morado extends Color{

	private String colorPintura= "Morado";
	
	public void pintar(Concesionario coche, Color pintura) {

		coche.setColor(pintura.getColor());

	}
	
	public String getColor() {
		return colorPintura;
	}
}
