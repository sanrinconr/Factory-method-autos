package Pintura;

import Marca.Concesionario;

public class Verde extends Color {

	private String colorPintura= "Verde";
	private String tipoPintura;
	
	public void pintar(Concesionario coche, Color pintura) {

		coche.setColor(pintura.getColor());

	}
	
	public String getColor() {
		return colorPintura;
	}
}